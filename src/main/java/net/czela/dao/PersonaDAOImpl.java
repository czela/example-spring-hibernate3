package net.czela.dao;

import java.util.List;

import net.czela.domain.PersonaBean;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PersonaDAOImpl implements PersonaDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void addPersona(PersonaBean persona) {
		sessionFactory.getCurrentSession().save(persona);
	}


	public List<PersonaBean> listaPersonas() {
		return (List<PersonaBean>)sessionFactory.getCurrentSession().createQuery("from persona").list();
	}

	public void deletePersona(Integer idPersona) {
		PersonaBean persona = (PersonaBean)sessionFactory.getCurrentSession().load(
				PersonaBean.class, idPersona);
		
		if(persona != null){
			sessionFactory.getCurrentSession().delete(persona);
		}
		
	}

}
