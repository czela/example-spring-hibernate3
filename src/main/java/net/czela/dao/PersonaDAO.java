package net.czela.dao;

import java.util.List;

import net.czela.domain.PersonaBean;

public interface PersonaDAO {
	
	public void addPersona(PersonaBean persona);
	public List<PersonaBean> listaPersonas ();
	public void deletePersona(Integer idPersona);
	

}
