package net.czela.controller;

import java.util.Map;

import net.czela.domain.PersonaBean;
import net.czela.service.PersonaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PersonaController {
	
	@Autowired
	private PersonaService personaService;
	
	@RequestMapping("/index")
	public String listaPersonas(Map<String, Object> map){
		map.put("persona", new PersonaBean());
		map.put("personaList", personaService.listaPersonas());
		return "persona";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addPersona(@ModelAttribute("persona") PersonaBean persona,
			BindingResult result){
		personaService.addPersona(persona);
		return "redirect:/index";
	}
	
	@RequestMapping("/delete/{personaId}")
	public String deletePersona(@PathVariable("personaId") Integer personaId){
		personaService.deletePersona(personaId);
		return "redirect:/index";
	}

}
