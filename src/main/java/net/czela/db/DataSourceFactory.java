package net.czela.db;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@SuppressWarnings("rawtypes")
public class DataSourceFactory implements FactoryBean, InitializingBean{
	
	//configuracion de properties
	
	private String dataBaseName;
	private Resource schemaLocation;
	private Resource dataLocation;
	private DataSource dataSource;
	
	//constructores
	public DataSourceFactory() {
	}

	public DataSourceFactory(String dataBaseName, Resource schemaLocation,
			Resource dataLocation) {
		setDataBaseName(dataBaseName);
		setSchemaLocation(schemaLocation);
		setDataLocation(dataLocation);
	}
	
	
	public void setDataBaseName(String dataBaseName) {
		this.dataBaseName = dataBaseName;
	}



	public void setSchemaLocation(Resource schemaLocation) {
		this.schemaLocation = schemaLocation;
	}



	public void setDataLocation(Resource dataLocation) {
		this.dataLocation = dataLocation;
	}



	public void afterPropertiesSet() throws Exception {
		if (dataBaseName == null) {
			throw new IllegalArgumentException("The name of the test database to create is required");
		}
		if (schemaLocation == null) {
			throw new IllegalArgumentException("The path to the database schema DDL is required");
		}
		if (dataLocation == null) {
			throw new IllegalArgumentException("The path to the test data set is required");
		}
		initDataSource();
		
	}

	private void initDataSource() {
		// create the in-memory database source first
		this.dataSource = createDataSource();
		
		System.out.println("Created in-memory test database '" + dataBaseName + "'");
		System.out.println("Exported schema in " + schemaLocation);
		
		// now populate the database by loading the schema and test data
		populateDataSource();
		System.out.println("Loaded test data in " + dataLocation);		
	}



	private void populateDataSource() {
		TestDatabasePopulator populator = new TestDatabasePopulator(dataSource);
		populator.populate();
	}



	private DataSource createDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		// use the HsqlDB JDBC driver
		dataSource.setDriverClassName("org.hsqldb.jdbcDriver");
		// have it create an in-memory database
		dataSource.setUrl("jdbc:hsqldb:mem:" + dataBaseName);
		dataSource.setUsername("sa");
		dataSource.setPassword("");
		return dataSource;
	}
	
	public Object getObject() throws Exception {
		return getDataSource();
	}
	
	public static DataSource createDataSource(String dataBaseName, Resource schemaLocation,
			Resource dataLocation) {
		return new DataSourceFactory(dataBaseName, schemaLocation, dataLocation).getDataSource();
	}

	public DataSource getDataSource() {
		if(dataSource == null){
			initDataSource();
		}
		return dataSource;
	}



	public Class getObjectType() {
		return DataSource.class;
	}

	public boolean isSingleton() {
		return true;
	}


	/**
	 * Populates a in memory data source with test data.
	 */
	private class TestDatabasePopulator {

		private DataSource dataSource;

		/**
		 * Creates a new test database populator.
		 * @param dataSource the test data source that will be populated.
		 */
		public TestDatabasePopulator(DataSource dataSource) {
			this.dataSource = dataSource;
		}

		/**
		 * Populate the test database by creating the database schema from 'schema.sql' and inserting the test data in
		 * 'testdata.sql'.
		 */
		public void populate() {
			Connection connection = null;
			try {
				connection = dataSource.getConnection();
				createDatabaseSchema(connection);
				insertTestData(connection);
			} catch (SQLException e) {
				throw new RuntimeException("SQL exception occurred acquiring connection", e);
			} finally {
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e) {
					}
				}
			}
		}

		// create the application's database schema (tables, indexes, etc.)
		private void createDatabaseSchema(Connection connection) {
			try {
				String sql = parseSqlIn(schemaLocation);
				executeSql(sql, connection);
			} catch (IOException e) {
				throw new RuntimeException("I/O exception occurred accessing the database schema file", e);
			} catch (SQLException e) {
				throw new RuntimeException("SQL exception occurred exporting database schema", e);
			}
		}

		// populate the tables with test data
		private void insertTestData(Connection connection) {
			try {
				String sql = parseSqlIn(dataLocation);
				executeSql(sql, connection);
			} catch (IOException e) {
				throw new RuntimeException("I/O exception occurred accessing the test data file", e);
			} catch (SQLException e) {
				throw new RuntimeException("SQL exception occurred loading test data", e);
			}
		}

		// utility method to read a .sql txt input stream
        private String parseSqlIn(Resource resource) throws IOException {
			InputStream is = null;
			try {
				is = resource.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				
				StringWriter sw = new StringWriter();
				BufferedWriter writer = new BufferedWriter(sw);
			
				for (int c=reader.read(); c != -1; c=reader.read()) {
					writer.write(c);
				}
				writer.flush();
				return sw.toString();
				
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}
		// utility method to run the parsed sql
		private void executeSql(String sql, Connection connection) throws SQLException {
			Statement statement = connection.createStatement();
			statement.execute(sql);
		}
	}

}
