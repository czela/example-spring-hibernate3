drop table persona if exists;
drop sequence seq if exists;

create table persona (id integer identity primary key, nombres varchar(100) not null, apellidos varchar(100) not null, direccion varchar(150), telefono integer);
create sequence seq start with 1 increment by 1;
