package net.czela.service;

import java.util.List;

import net.czela.dao.PersonaDAO;
import net.czela.domain.PersonaBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonaServiceImpl implements PersonaService{
	
	@Autowired
	private PersonaDAO personaDao;

	@Transactional
	public void addPersona(PersonaBean persona) {
		personaDao.addPersona(persona);
		
	}

	@Transactional
	public List<PersonaBean> listaPersonas() {
		return personaDao.listaPersonas();
	}

	@Transactional
	public void deletePersona(Integer idPersona) {
		personaDao.deletePersona(idPersona);
	}
	

}
