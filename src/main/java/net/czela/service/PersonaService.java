package net.czela.service;

import java.util.List;

import net.czela.domain.PersonaBean;

public interface PersonaService {
	
	public void addPersona(PersonaBean persona);
	public List<PersonaBean> listaPersonas();
	public void deletePersona(Integer idPersona);

}
