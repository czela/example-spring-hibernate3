<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring 3/czela - Persona Manager</title>
<style type="text/css">
body {
	font-family: sans-serif;
}

.data,.data td {
	border-collapse: collapse;
	width: 100%;
	border: 1px solid #aaa;
	margin: 2px;
	padding: 2px;
}

.data th {
	font-weight: bold;
	background-color: #5C82FF;
	color: white;
}
</style>
</head>
<body>
	<h2>Person Manager</h2>

	<form:form method="post" action="add.html" commandName="persona">

		<table>
			<tr>
				<td><form:label path="nombres">
						Nombre Completo
					</form:label></td>
				<td><form:input path="nombres" /></td>
			</tr>
			<tr>
				<td><form:label path="apellidos">
						Apellidos
					</form:label></td>
				<td><form:input path="apellidos" /></td>
			</tr>
			<tr>
				<td><form:label path="direccion">
						Direccion
					</form:label></td>
				<td><form:input path="direccion" /></td>
			</tr>
			<tr>
				<td><form:label path="telefono">
						Telefono
					</form:label></td>
				<td><form:input path="telefono" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Agregar Persona" />
				</td>
			</tr>
		</table>
	</form:form>

	<h3>Personas</h3>
	<c:if test="${!empty personaList}">
		<table class="data">
			<tr>
				<th>Nombre Completo</th>
				<th>Direccion</th>
				<th>Telefono</th>
				<th></th>
			</tr>
			<c:forEach items="${personaList}" var="persona">
				<tr>
					<td>${persona.apellidos}, ${persona.nombres}</td>
					<td>${persona.direccion}</td>
					<td>${persona.telefono}</td>
					<td><a href="delete/${persona.id}">delete</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</c:if>


</body>
</html>